import Graph from "graphology";
import { Attributes } from "graphology-types";

export type ImportableNode = {
  id: unknown;
  attributes?: Attributes | undefined;
};

export type ImportableEdge = {
  from: unknown;
  to: unknown;
  attributes?: Attributes | undefined;
};

async function importNodes(
  graph: Graph,
  nodes: unknown[],
  nodeAccessor: (node: any) => ImportableNode
) {
  nodes.forEach((node: unknown) => {
    const nodeData = nodeAccessor(node);
    console.log(nodeData);

    const insertedNode = graph.addNode(nodeData.id, nodeData.attributes);

    // console.log(insertedNode, graph.order);
  });
}

async function importEdges(
  graph: Graph,
  edges: unknown[],
  edgeAccessor: (edge: any) => ImportableEdge
) {
  edges.forEach((edge: unknown) => {
    const edgeData = edgeAccessor(edge);
    console.log(edgeData);

    const insertedEdge = graph.addEdge(
      edgeData.from,
      edgeData.to,
      edgeData.attributes
    );
    // console.log(insertedEdge, graph.size);
  });
}

function createGraph() {
  const graph = new Graph({ multi: true, allowSelfLoops: false });
  return graph;
}

export { importNodes, importEdges, createGraph };
