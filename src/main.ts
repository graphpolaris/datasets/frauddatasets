import Graph from "graphology";
import { insuranceFraudData } from "./insurance-fraud-data"; // Assuming you have a data.ts file with your array
import { saveDataToNeo4jEdges, saveDataToNeo4jNodes } from "./neo4j";
import {
  ImportableEdge,
  ImportableNode,
  createGraph,
  importEdges,
  importNodes,
} from "./graphphology-importer";
import { Connection, ImportEntity } from "./entity-data";
import { bankFraudData } from "./bank-fraud-data";
import { assert } from "console";

// async function main() {
//   try {
//     // console.log(insuranceFraudData.nodesSource);

//     // const neo4jData = insuranceFraudData.nodesSource.map((item) => ({ name: item., age: item.age }));
//     // await saveDataToNeo4jNodes(insuranceFraudData.nodesSource);

//     // insuranceFraudData.nodesSource.forEach((data) => {
//     //   console.log(data.id, data.type, data.enter, data.exit, data.info);
//     // });

//     const graph = createGraph();
//     importNodes(graph, insuranceFraudData.nodesSource, nodeAccessor);
//     console.log("Data imported to Graphphology graph successfully.");

//     console.log(graph.order, graph.size, graph.nodes(), graph.edges());
//   } catch (error) {
//     console.error("Error importing data to Neo4j:", error);
//   }
// }

async function main() {
  try {
    // console.log(insuranceFraudData.nodesSource);

    // const neo4jData = insuranceFraudData.nodesSource.map((item) => ({ name: item., age: item.age }));
    // await saveDataToNeo4jNodes(insuranceFraudData.nodesSource);

    // insuranceFraudData.nodesSource.forEach((data) => {
    //   console.log(data.id, data.type, data.enter, data.exit, data.info);
    // });

    const steps = 5;

    const graph = createGraph();
    importNodes(graph, bankFraudData.nodesSource, nodeAccessor);
    console.log(
      "1/" + steps + " Node data imported to graphology model successfully."
    );
    assert(
      graph.order === bankFraudData.nodesSource.length,
      "Node data import failed."
    );

    importEdges(graph, bankFraudData.edgesSource, edgeAccessor);
    console.log(
      "2/" + steps + " edge data imported to graphology model successfully."
    );
    assert(
      graph.size === bankFraudData.edgesSource.length,
      "Edge data import failed."
    );

    console.log(
      "3/" + steps + " Data imported to Graphphology graph successfully."
    );
    console.log(graph.order, graph.size, graph.nodes(), graph.edges());

    console.log(
      "data schema to import:" + JSON.stringify(bankFraudData.nodesSource[0])
    );

    const importQueryNodes = `
      MERGE (n:$type {id: $id, type: $type, enter: $enter, exit: $exit}) SET n += $info
    `;
    console.log(
      "data schema to import nodes:" +
        JSON.stringify(bankFraudData.nodesSource[0] + "\r\n" + importQueryNodes)
    );
    await saveDataToNeo4jNodes(bankFraudData.nodesSource, importQueryNodes, ["info"]);

    // const importQueryEdges = `
    //   UNWIND $data AS item
    //   MERGE (n:item.type {id: item.id, type: item.type, enter: item.enter, exit: item.exit, info: item.info})
    // `;

    // console.log(
    //   "data schema to import edges:" +
    //     JSON.stringify(bankFraudData.edgesSource[0] + "\r\n" + importQueryEdges)
    // );
    // await saveDataToNeo4jEdges(bankFraudData.edgesSource, importQueryEdges);
  } catch (error) {
    console.error("Error importing data to Neo4j:", error);
  }
}

function nodeAccessor(node: ImportEntity): ImportableNode {
  return {
    id: node.id,

    attributes: {
      type: node.type,
      enter: node.enter,
      exit: node.exit,
      info: node.info,
    },
  };
}

function edgeAccessor(edge: Connection): ImportableEdge {
  return {
    from: edge.from,
    to: edge.to,

    attributes: {
      type: edge.type,
      fraud: edge.fraud,
    },
  };
}

main();
