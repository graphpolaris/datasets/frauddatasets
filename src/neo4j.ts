// neo4j.ts

import neo4j from "neo4j-driver";
import * as dotenv from "dotenv";
import { Connection, ImportEntity } from "./entity-data";

// Load environment variables from .env file
dotenv.config();

const driver = neo4j.driver(
  process.env.NEO4J_CONNECTION_STRING as string, // Neo4j server URL
  neo4j.auth.basic(
    process.env.NEO4J_USERNAME as string,
    process.env.NEO4J_PASSWORD as string
  ) // Neo4j username and password, cypher-shell -a neo4j+s://neo4j.staging.graphpolaris.com:8000 -u neo4j -p 'pwd_omitted_here'
);

async function saveDataToNeo4jNodes(
  data: ImportEntity[],
  importQuery: string,
  flattening: string[]
) {
  if (data.length === 0) {
    return;
  }

  if (validityCheck(data, importQuery, flattening) == false) {
    return;
  }

  const session = driver.session();

  // const query = `
  //   UNWIND $data AS item
  //   MERGE (n:Person {name: item.name})
  //   SET n.age = item.age
  // `;
  // await session.run(importQuery, { data });

  for (let i = 0; i < data.length; i++) {
    // const inputObject = data[i];
    // let infoObject: { [key: string]: string } | any;

    // // Check the type of info property and convert it to an object if it's a string
    // if (typeof inputObject.info === "string") {
    //   infoObject = { info: inputObject.info };
    // } else {
    //   infoObject = inputObject.info;
    // }

    // // Merge the infoObject with the rest of the properties
    // const node = {
    //   ...inputObject,
    //   ...infoObject,
    // };
    const node = data[i] as any;
    let importQueryNode = importQuery.replace(
      "$type",
      node.type.replace(" ", "")
    );

    for (let j = 0; j < flattening.length; j++) {
      const key = flattening[j];
      if (node[key]) {
        const flatteningObj = JSON.stringify(node[key]);
        importQueryNode += ` SET ${flatteningObj}`;

        delete node[key];
      }
    }

    // console.log(importQueryNode);

    // await session.executeWrite(async (tx) => {
    //   await tx.run(importQuery, node);
    // });
  }
  await session.close();
}

async function saveDataToNeo4jEdges(data: Connection, importQuery: string) {
  const session = driver.session();

  try {
    // const query = `
    //   UNWIND $data AS item
    //   MERGE (n:Person {name: item.name})
    //   SET n.age = item.age
    // `;
    // await session.run(importQuery, { data });
  } finally {
    session.close();
  }
}

function validityCheck(
  data: ImportEntity[],
  importQuery: string,
  flattening: string[]
) {
  // if `£` follows non-digit also then use
  console.log("querymatch", importQuery.match(/$\S+/g));
  for (let i = 0; i < data.length; i++) {}
}
export { saveDataToNeo4jNodes, saveDataToNeo4jEdges };
